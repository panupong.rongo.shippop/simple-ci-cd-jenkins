package main

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

// SetupRouter make for setup go routing
func SetupRouter() *gin.Engine {
	router := gin.Default()
	router.GET("/", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{
			"hello": "world",
		})
	})

	router.GET("/benzene/85", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{
			"octane": 85,
		})
	})

	router.GET("/benzene20", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{
			"octane2": 20,
		})
	})

	router.GET("/benzene10", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{
			"octane2": 10,
		})
	})
	router.GET("/disel", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{
			"disel": 5,
		})
	})

	return router
}

func main() {
	r := SetupRouter()
	r.Run() // listen and serve on 0.0.0.0:8080 (for windows "localhost:8080")
}
